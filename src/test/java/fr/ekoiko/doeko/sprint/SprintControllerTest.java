package fr.ekoiko.doeko.sprint;

import fr.ekoiko.doeko.config.Application;
import fr.ekoiko.doeko.project.Project;
import fr.ekoiko.doeko.project.ProjectRepository;
import fr.ekoiko.doeko.sprint.Sprint;
import fr.ekoiko.doeko.sprint.SprintRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest({"server.port=0"})
public class SprintControllerTest {
    private static boolean populateDone = false;

    @Value("${local.server.port}")
    private int port;

    private RestTemplate template;

    @Autowired
    private SprintRepository sprintRepository;
    @Autowired
    private ProjectRepository projectRepository;


    @Before
    public void setUp() throws Exception {
        template = new TestRestTemplate();
        if(!populateDone) {
            populateRepositories();
            populateDone = true;
        }
    }

    private void populateRepositories(){
        template = new TestRestTemplate();
    }

    @Test
    public void shouldCreateSprintAndAddToProjectFFT()throws Exception {
        Project fft = new Project("FFT");
        projectRepository.save(fft);

        int nbSprintBefore = sprintRepository.findAll().size();
        int nbSprintsByProjectBefore = fft.getSprints().size();


        URL url = new URL("http://localhost:"+ port + "/projet/1/sprint");
        Map<String, Object> data = new HashMap<>();
        ResponseEntity<String> response = template.postForEntity(url.toString(), data, String.class);

        fft = projectRepository.findOne(1);
        assertEquals("Should add +1 to sprintRepository size", nbSprintBefore + 1, sprintRepository.findAll().size());
        assertEquals("Should add +1 to project Sprints size", nbSprintsByProjectBefore + 1, fft.getSprints().size());
    }
}
