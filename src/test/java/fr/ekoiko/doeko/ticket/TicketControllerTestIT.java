package fr.ekoiko.doeko.ticket;

import fr.ekoiko.doeko.config.Application;
import fr.ekoiko.doeko.project.Project;
import fr.ekoiko.doeko.project.ProjectRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest({"server.port=0"})
public class TicketControllerTestIT {
    private static boolean populateDone = false;

    @Value("${local.server.port}")
    private int port;

    private RestTemplate template;

    @Autowired
    private ProjectRepository repository;
    @Autowired
    private TicketRepository ticketRepository;

    @Before
    public void setUp() throws Exception {
        template = new TestRestTemplate();
        if(!populateDone) {
            populateRepositories();
            populateDone = true;
        }
    }

    private void populateRepositories() {
        Project doEko = new Project("DoEko");
        Project fft = new Project("FFT");
        Project dealPotAge = new Project("DealPotAge");

        repository.save(doEko);
        repository.save(fft);
        repository.save(dealPotAge);

        Ticket t1 = new Ticket("T1", "Ticket test 1");
        Ticket t2 = new Ticket("T2", "Ticket test 2");
        ticketRepository.save(t1);
        ticketRepository.save(t1);
        doEko.addTicket(t1);
        doEko.addTicket(t2);
    }

    @Test
    public void shouldCreateATicket() throws MalformedURLException {
        int nbTicketsBefore = ticketRepository.findAll().size();
        String title = "Test Creation";
        String description = "Ticket test creation";

        URL url = new URL("http://localhost:" + port + "/projet/1/ticket");
        Map<String, Object> payload = new HashMap<>();
        payload.put("title", title);
        payload.put("description", description);
        ResponseEntity<Ticket> response = template.postForEntity(url.toString(), payload, Ticket.class);

        assertEquals("Returned ticket should have same title", title, response.getBody().getTitle());
        assertEquals("Returned ticket should have same description", description, response.getBody().getDescription());
        assertEquals("A ticket should have been created", nbTicketsBefore + 1, ticketRepository.findAll().size());

        Project project = repository.findOne(1);
        List<Ticket> backlog = project.getBacklog();
        assertTrue("Ticket should have been added to backlog", ticketIsInBacklog(title, backlog));
    }

    @Test
    public void shouldUpdateTicket()throws MalformedURLException{
        int nbTicketsBefore = ticketRepository.findAll().size();
        Ticket ticket = ticketRepository.findOne(1);
        String stateStart = ticket.getState().toString();
        String stateEnd = "Terminé";
        URL url = new URL("http://localhost:" + port + "/projet/1/ticket/1");
        Map<String, Object> payload = new HashMap<>();
        payload.put("state", "Terminé");
        template.put(url.toString(), payload, ticket);

        Ticket updatedTicket = ticketRepository.findOne(1);
        assertEquals("Returned ticket should have same title", ticket.getTitle(), updatedTicket.getTitle());
        assertEquals("Returned ticket state should be Done", TicketState.DONE, updatedTicket.getState());
        assertEquals("A ticket should have been created", nbTicketsBefore, ticketRepository.findAll().size());
    }

    private boolean ticketIsInBacklog(String title, List<Ticket> backlog){
        for(Ticket t : backlog){
            if(t.getTitle().equals(title)){
                return true;
            }
        }
        return false;
    }
}
