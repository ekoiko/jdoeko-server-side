package fr.ekoiko.doeko.user;


import fr.ekoiko.doeko.config.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest({"server.port=0"})
public class UserControllerTest {
    private static boolean populateDone = false;

    @Value("${local.server.port}")
    private int port;

    private RestTemplate template;

    @Autowired
    private UserRepository repository;


    @Before
    public void setUp() throws Exception {
        template = new TestRestTemplate();
        if(!populateDone) {
            populateRepositories();
            populateDone = true;
        }
    }

    private void populateRepositories(){
        template = new TestRestTemplate();
        User johan = new User("Johan","Ravry","johan.ravry@laposte.net","toto");
        User quentin = new User("Quentin","Choulet","quentin.choulet@gmail.com","toto");
        repository.save(johan);
        repository.save(quentin);
    }



    @Test
    public void user1IsJohan() throws Exception {
        URL url = new URL("http://localhost:"+ port + "/user/1");
        ResponseEntity<String> response = template.getForEntity(url.toString(), String.class);
        System.out.println(response.getBody());
        assertThat(response.getBody(), equalTo("{\"id\":1,\"firstName\":\"Johan\",\"lastName\":\"Ravry\",\"email\":\"johan.ravry@laposte.net\",\"password\":\"pass\"}"));
    }

    @Test
    public void testInscription() throws Exception {
        String email = "teo.cannibal@gmail.com";
        Map<String, Object> teo = new HashMap<>();
        teo.put("firstName","Teo");
        teo.put("lastName","Cannibal");
        teo.put("email",email);
        teo.put("password","password");
        URL url = new URL("http://localhost:"+ port + "/user");

        ResponseEntity<String> response = template.postForEntity(url.toString(), teo, String.class);
        User userTest = repository.findByEmail(email);
        assertTrue(userTest.getEmail().equals(email));
    }
}
