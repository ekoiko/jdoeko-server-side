package fr.ekoiko.doeko.project;

import fr.ekoiko.doeko.config.Application;
import fr.ekoiko.doeko.user.User;
import fr.ekoiko.doeko.user.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest({"server.port=0"})
public class ProjectControllerTestIT {
    private static boolean populateDone = false;

    @Value("${local.server.port}")
    private int port;

    private RestTemplate template;

    @Autowired
    private ProjectRepository repository;
    @Autowired
    private UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
        template = new TestRestTemplate();
        if(!populateDone) {
            populateRepositories();
            populateDone = true;
        }
    }

    private void populateRepositories() {
        User johan = new User("Johan", "Ravry", "johan.ravry@laposte.net", "pass");
        User quentin = new User("Quentin", "Choulet", "quentin.choulet@gmail.com", "pass");
        userRepository.save(johan);
        userRepository.save(quentin);

        Project fft = new Project("FFT");
        Project doEko = new Project("DoEko");
        Project dealPotAge = new Project("DealPotAge");

        doEko.addMember(johan);
        doEko.addMember(quentin);
        dealPotAge.addMember(quentin);

        repository.save(fft);
        repository.save(doEko);
        repository.save(dealPotAge);
    }

    @Test
    public void shouldGetProjectFromDatabase() throws Exception {
        URL url = new URL("http://localhost:" + port + "/projet/1");
        ResponseEntity<String> response = template.getForEntity(url.toString(), String.class);
        assertThat(response.getBody(), equalTo("{\"id\":1,\"name\":\"FFT\",\"members\":[],\"sprints\":[],\"backlog\":[]}"));
    }

    @Test
    public void shouldGetUserProjectsFromDatabase() throws Exception {
        URL url = new URL("http://localhost:" + port + "/user/1/projets");
        ResponseEntity<String> response = template.getForEntity(url.toString(), String.class);
        String expected = "[{\"id\":2,\"name\":\"DoEko\",\"members\":"+
                                "[{\"id\":1,\"firstName\":\"Johan\",\"lastName\":\"Ravry\",\"email\":\"johan.ravry@laposte.net\",\"password\":\"pass\"},"+
                                "{\"id\":2,\"firstName\":\"Quentin\",\"lastName\":\"Choulet\",\"email\":\"quentin.choulet@gmail.com\",\"password\":\"pass\"}],\"sprints\":[],\"backlog\":[]}]";

        assertThat(response.getBody(), equalTo(expected));
    }

    @Test
    public void shouldCreateANewProject() throws Exception{
        int nbProjectsBefore = repository.findAll().size();

        List<String> members = new ArrayList<>();
        members.add("quentin.choulet@gmail.com");
        String name = "MyProject";

        URL url = new URL("http://localhost:" + port + "/projet");
        Map<String, Object> payload = new HashMap<>();
        payload.put("name", name);
        payload.put("members", members);
        ResponseEntity<Project> response = template.postForEntity(url.toString(), payload, Project.class);

        assertEquals("Should create a new project", nbProjectsBefore + 1, repository.findAll().size());
        Project newProject = repository.findOne(response.getBody().getId());
        assertNotEquals("Should find the new project", newProject, null);
        assertEquals("Name should be " + name, name, newProject.getName());
        assertEquals("Project should have 1 member", newProject.getMembers().size(), 1);
        assertEquals("Member should be Quentin", newProject.getMembers().get(0).getFirstName(), "Quentin");
        assertEquals("Member should be Quentin", newProject.getSprints().size(), 1);
    }
}
