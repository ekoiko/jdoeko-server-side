package fr.ekoiko.doeko.ticket;


public enum TicketState {
    BACKLOG("Backlog"),
    TO_DO("A faire"),
    DOING("En cours"),
    DONE("Terminé");

    private String name;

    TicketState(String name) {
        this.name = name;
    }

    public String toString(){
        return this.name;
    }
}
