package fr.ekoiko.doeko.ticket;

import fr.ekoiko.doeko.exceptions.ResourceNotFoundException;
import fr.ekoiko.doeko.project.Project;
import fr.ekoiko.doeko.project.ProjectController;
import fr.ekoiko.doeko.project.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
public class TicketController {

    private TicketRepository ticketRepository;
    private ProjectRepository projectRepository;

    @Autowired(required = true)
    public TicketController (TicketRepository ticketRepository, ProjectRepository projectRepository){
        this.ticketRepository = ticketRepository;
        this.projectRepository = projectRepository;
    }

    @RequestMapping(value = "/projet/{projectId}/ticket", method = RequestMethod.POST)
    public Ticket createTicket(@PathVariable String projectId, @RequestBody Map<String, Object> payload){
        Project project = projectRepository.findOne(Integer.parseInt(projectId));

        if(project == null){
            throw new ResourceNotFoundException();
        }

        Ticket newTicket = new Ticket();
        newTicket.setTitle(payload.get("title").toString());
        newTicket.setDescription(payload.get("description").toString());
        Ticket createdTicket = ticketRepository.saveAndFlush(newTicket);

        project.addTicket(newTicket);
        projectRepository.saveAndFlush(project);

        return createdTicket;
    }

    @RequestMapping(value = "/projet/{projectId}/ticket/{ticketId}", method = RequestMethod.PUT)
    public Ticket updateTicketState(@PathVariable String projectId, @PathVariable Integer ticketId, @RequestBody Map<String, Object> payload){
        Ticket ticket = ticketRepository.findOne(ticketId);
        if(ticket == null){
            throw new ResourceNotFoundException();
        }

        TicketState newState;
        for (int i=0;i<TicketState.values().length;i++) {
            if (payload.get("state").toString().equals(TicketState.values()[i].toString())) {
                newState = TicketState.values()[i];
                ticket.setState(newState);
                ticketRepository.save(ticket);
                break;
            }
        }

        return ticket;
    }
}
