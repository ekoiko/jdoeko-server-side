package fr.ekoiko.doeko.user;

import fr.ekoiko.doeko.exceptions.ResourceNotFoundException;
import fr.ekoiko.doeko.exceptions.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    private final UserRepository userRepository;

    @Autowired(required = true)
    public UserController(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public User userById(@PathVariable String id){

        User user = userRepository.findOne(Integer.parseInt(id));

        if (user == null){
            System.out.println("User null mais ça marche");
        }

        return user;
    }

    /**
     * Fake login, cherche un utilisateur correspondant au login/mdp envoyé en paramètre
     * et renvoit l'utilisateur correspondant s'il existe
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public User login(@RequestBody Map<String, String > credentials){
        String email = credentials.get("email");
        String password = credentials.get("password");

        User user = userRepository.findByEmail(email);
        if(user == null){
            throw new UnauthorizedException();
        }
        else if(user.getPassword().equals(password)){
            return user;
        }
        else{
            throw new UnauthorizedException();
        }
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> users(){

        List<User> users = userRepository.findAll();

        if (users == null){
            System.out.println("User null mais ça marche");
        }

        return users;
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public User inscription(@RequestBody Map<String, Object> user){
        User newUser = new User(user.get("firstName").toString(),user.get("lastName").toString(),user.get("email").toString(),user.get("password").toString());
        userRepository.save(newUser);

        return newUser;
    }
}
