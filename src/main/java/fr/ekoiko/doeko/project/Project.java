package fr.ekoiko.doeko.project;

import fr.ekoiko.doeko.sprint.Sprint;
import fr.ekoiko.doeko.sprint.SprintRepository;
import fr.ekoiko.doeko.ticket.Ticket;
import fr.ekoiko.doeko.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;

    @ManyToMany(fetch=FetchType.EAGER)
    private List<User> members;

    @OneToMany(fetch=FetchType.EAGER)
    private List<Sprint> sprints;

    @OneToMany(fetch=FetchType.EAGER)
    private List<Ticket> backlog;

    protected Project() {
        this.members = new ArrayList<>();
        this.sprints = new ArrayList<>();
        this.backlog = new ArrayList<>();
    }

    public Project(String name){
        this();
        this.name = name;
    }

    public List<User> getMembers() {
        return members;
    }

    public void addMember (User member){
        this.members.add(member);
    }

    public void addMembers (List<User> members){
        this.members.addAll(members);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void addSprint(Sprint sprint){ this.sprints.add(sprint); }

    public List<Sprint> getSprints() { return sprints; }

   /* public Sprint getLatestSprint(){
        if(sprints.size() > 0){
            return sprints.get(sprints.size()-1);
        }
        return null;

    }*/

    public void setSprints(List<Sprint> sprints) { this.sprints = sprints; }

    public void addTicket(Ticket newTicket) {
        this.backlog.add(newTicket);
    }

    public List<Ticket> getBacklog() {
        return backlog;
    }

    public void setBacklog(List<Ticket> backlog) {
        this.backlog = backlog;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }
}
