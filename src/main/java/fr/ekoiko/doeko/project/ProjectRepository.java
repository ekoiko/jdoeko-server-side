package fr.ekoiko.doeko.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface ProjectRepository extends JpaRepository<Project, Integer>{
    String FIND_USER_PROJECTS_QUERY =
            "SELECT p " +
            "FROM Project p JOIN p.members m " +
            "WHERE m.id = :userId";

    @Query(FIND_USER_PROJECTS_QUERY)
    List<Project> findUserProjects(@Param("userId") Integer userId);
}
