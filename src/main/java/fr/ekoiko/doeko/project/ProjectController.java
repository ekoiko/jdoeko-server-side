package fr.ekoiko.doeko.project;

import fr.ekoiko.doeko.exceptions.ResourceNotFoundException;
import fr.ekoiko.doeko.sprint.Sprint;
import fr.ekoiko.doeko.sprint.SprintRepository;
import fr.ekoiko.doeko.user.User;
import fr.ekoiko.doeko.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class ProjectController {

    private final ProjectRepository projectRepository;
    private final UserRepository userRepository;
    private final SprintRepository sprintRepository;

    @Autowired(required = true)
    public ProjectController(ProjectRepository projectRepository, UserRepository userRepository, SprintRepository sprintRepository) {
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
        this.sprintRepository = sprintRepository;
    }

    @RequestMapping(value = "/projet/{id}", method = RequestMethod.GET)
    public Project getProjectById(@PathVariable String id){
        Project project = projectRepository.findOne(Integer.parseInt(id));
        if(project == null){
            throw new ResourceNotFoundException();
        }
        return project;
    }

    @RequestMapping(value = "/user/{userId}/projets", method = RequestMethod.GET)
    public List<Project> getUserProjects(@PathVariable String userId){
        List<Project> projects = projectRepository.findUserProjects(Integer.parseInt(userId));
        return projects;
    }

    @RequestMapping(value = "/projet", method = RequestMethod.POST)
    public Project createProject(@RequestBody Map<String, Object> payload){
        Project project = new Project();
        project.setName((String) payload.get("name"));

        List<String> membersEmails = (List) payload.get("members");
        List<User> members = userRepository.findByEmail(membersEmails);
        project.addMembers(members);
        Sprint newSprint = new Sprint();
        sprintRepository.save(newSprint);
        project.addSprint(newSprint);

        Project newProject = projectRepository.saveAndFlush(project);
        return newProject;
    }
}
