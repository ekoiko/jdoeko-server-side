package fr.ekoiko.doeko.sprint;

import fr.ekoiko.doeko.exceptions.ResourceNotFoundException;
import fr.ekoiko.doeko.project.Project;
import fr.ekoiko.doeko.project.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SprintController {

    private final SprintRepository sprintRepository;
    private final ProjectRepository projectRepository;

    @Autowired(required = true)
    public SprintController(SprintRepository sprintRepository, ProjectRepository projectRepository){
        this.sprintRepository = sprintRepository;
        this.projectRepository = projectRepository;
    }

    @RequestMapping(value = "/projet/{idProject}/sprint/{idSprint}", method = RequestMethod.GET)
    public Sprint getSprintById(@PathVariable String idSprint){
        Sprint sprint = sprintRepository.findOne(Integer.parseInt(idSprint));
        if(sprint == null){
            throw new ResourceNotFoundException();
        }
        return sprint;
    }

    @RequestMapping(value = "/projet/{idProject}/sprint", method = RequestMethod.POST)
    public Sprint createSprint(@PathVariable String idProject){
        Project project = projectRepository.findOne(Integer.parseInt(idProject));

        if(project == null){
            throw new ResourceNotFoundException();
        }else{
            Sprint newSprint = new Sprint();
            Sprint sprint = sprintRepository.saveAndFlush(newSprint);

            project.addSprint(newSprint);
            projectRepository.saveAndFlush(project);

            return sprint;
        }

    }
}
