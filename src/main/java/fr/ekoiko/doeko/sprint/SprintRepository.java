package fr.ekoiko.doeko.sprint;


import org.springframework.data.jpa.repository.JpaRepository;

public interface SprintRepository extends JpaRepository<Sprint, Integer>{
}
